import React, { useState, useEffect } from "react";
import { Routes, Route, Navigate, Switch, Link } from "react-router-dom";
import MainOutlet from "./component/MainOutlet";
import App from "./pages/login";
import Out from "./pages/Out";
import PlaceOrder from "./pages/PlaceOrder";
import Purchasing from "./pages/Purchasing";
import Purchasing1 from "./pages/Purchasing1";
import EnsureOrder from "./pages/EnsureOrder";
import Order from "./pages/Order";
import Home from "./pages/Home";
import MainOutletChoose from "./component/MainOutChoose";
import OrderRecord from "./pages/OrderRecord";
import Takeaway from "./pages/takeaway";
import Takeaway2 from "./pages/takeaway2";
import Register from "./pages/Register";
const MainRoutes = () => {
    return (
        <>
            <Routes>
                <Route path={""} element={<App />}></Route>
                <Route path={"/register"} element={<Register />}></Route>
                <Route path="/" element={<MainOutletChoose />}>
                    <Route path={"home"} element={<Home />}></Route>
                </Route>
                <Route path="/" element={<MainOutlet />}>
                    <Route path={"purchasing"} element={<Purchasing />}></Route>
                    <Route path={"purchasing1"} element={<Purchasing1 />}></Route>
                    <Route path={"ensureorder"} element={<EnsureOrder />}></Route>
                    <Route path={"out"} element={<Out />}></Route>
                    <Route path={"placeorder"} element={<PlaceOrder />}></Route>
                    <Route path={"order"} element={<Order />}></Route>
                    <Route path={"orderrecord"} element={<OrderRecord />}></Route>
                    <Route path={"takeaway"} element={<Takeaway />}></Route>
                    <Route path={"takeaway2"} element={<Takeaway2 />}></Route>
                    {/* <Route path={'poem'} element={<Poem />} ></Route> */}
                </Route>
            </Routes>
        </>
    );
};

export default MainRoutes;
