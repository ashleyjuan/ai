// // import logo from "./logo.svg";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Container, Col, Row, Form, Button, Modal, Table } from 'react-bootstrap';
import "./style.css";
function Register() {
    const [show, setShow] = useState(false)
    const navigate = useNavigate();
    const handleOnLogin = () => {
        navigate('/');
        setShow(false);
    }
    return (
        <>
            <div className="col" style={{ justifyContent: 'center' }}>
                <section className="loginBox">
                    <div className="row">
                        <div className="top col-12">
                            <h2 style={{ textAlign: 'center', backgroundColor: '#F6F8EE' }}>會員註冊</h2>
                        </div>
                    </div>
                    <section className="accountBox">
                        <p>姓名</p>
                        <input type="text" />
                    </section>
                    <section className="accountBox">
                        <p>帳號/信箱</p>
                        <input type="text" />
                    </section>
                    <section className="passwordBox">
                        <p>輸入密碼</p>
                        <input type="password" />
                    </section>
                    <section className="passwordBox">
                        <p>確認密碼</p>
                        <input type="password" />
                    </section>
                    <section className="accountBox">
                        <p>電話</p>
                        <input type="text" />
                    </section>
                    <div className="row justify-content-center my-4">
                        <div className="col-auto">
                            <button className="btn" style={{ background: '#EFF1E5', fontSize: '24px' }} onClick={e => navigate(-1)}>返回</button>
                        </div>
                        <div className="col-auto">
                            <button className="btn" style={{ background: '#EFF1E5', fontSize: '24px' }} onClick={e => setShow(true)}>註冊</button>
                        </div>
                    </div>

                </section>
            </div>
            <Modal show={show} onHide={e => setShow(false)}>
                <Modal.Header className='justify-content-center' style={{ background: '#F6F8EE' }}>
                    <Modal.Title style={{ fontSize: '24px' }}>註冊成功</Modal.Title>
                </Modal.Header>
                <Modal.Body >
                    <Col style={{ textAlign: 'center', padding: '30px', fontSize: '24px' }}>可進行登入囉～～</Col>
                    {/* <Modal.Footer className='justify-content-center'> */}
                    <Row className='justify-content-center'>
                        <Col md="auto">
                            <button className='btn' onClick={handleOnLogin} style={{ background: '#F6F8EE', color: 'black' }}>確認</button>
                        </Col>
                    </Row>
                </Modal.Body>
                {/* </Modal.Footer> */}
            </Modal>
        </>
    );
}

export default Register;
