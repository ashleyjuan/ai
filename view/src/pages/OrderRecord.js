import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Form, Button, Modal, Table } from 'react-bootstrap';
import { Navigate, Outlet, useNavigate, useLocation } from 'react-router-dom'
import styled from 'styled-components';
const OrderRecord = () => {
    const navigate = useNavigate();
    return (
        <>
            <Col md='auto'>
                <img onClick={e => navigate(-1)} type='button' width='auto' src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAAtCAYAAAD/aHgLAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMKSURBVHgB3ZpBTttAFIbfjE1ZlXIEHyHcgFwAYoUidYUilVRdNTlB4QSFFSpBSrpCagmTcAF6A3KD+ghhWdWe6TwnQXXwTJy4Is/+Nons8eLpef557/0G+A9ci67XH1wKIAyHnHRFd3uTR/eMsVp/2PkMRMkd6JbzpwugPPzPAE5uh1dHQBAGORA6g0oHN3d5HEpVPfSbI1gDP8TFbtr1lQPti68txvmX9Lsq+C2j6jv/YwAviBAXnuLur7R7K726KD46SMt+ZN4r4DUgxNKBYpAoPvrvtmmNBHVe9z+cASFcWJJNHuIx4plXqOBgv9kCYiyVUX184J6smFeogOm9CQTJHCgqrFYua6aYBN9/YQHKSqZAv4vLSsoxkkAxaPtrOlKysDBQFB+Xc2t5FwGc1veOSYnPPNZAZ+XdrPIxMHq7f3wCxLGq7mseYpY884pYfHwoAMZAp+WdrW4do8JSFZ95Ul/dG3FVWyg+UrWLEiTyLFAUH85V1/ZQLD5+swcFIhFolvKOMTUogvjMk9ijmyzETHqW9eMoUt9MrdC6keB4pnbs6bqhtywNcaD23rIcMCzvXM4eoOTwDYeVOpMzOESqrX/HUHI4dhy68ziFkvOkunpMqeta9QlKSuLYEXede6Vg1/qAZLqIV4V71ZNFfeT6wKMHW1smHXlU32sWomP5l0QJ6PsN3ZE4OPMxZowp2taDiWdFvQ42kJI1bA9Rth5MpLZpB/77gS4HFyixOsNiAwqC1ZK4GXZ63Np8q+BRbuw09CsPxLHOjBzp4njTMtlj3hsnJO2LzrAGOhEnFxXWnDF9HE0H26RZOO5EccLBtG0NDraxAwLCZLYN+3edFlNgzZz2RXcOiQ6xM1sSkwE1O7etcTmIa+1RAkGWNoJvhx3sXW3HyuhRulVqSry0PzoRJxZYllS2eEiuclrJ2hdoOnFmnRbquW+D0kh0JWs/7mFl3LAbUIHu6H8CIVb+/AazZSgTSVoVuT6/QeZ7WPRJKVqIuT+oinvYqTgVwSfNhdBWhj52ekCYvzQBKODfKil1AAAAAElFTkSuQmCC" />
            </Col>
            <Row className=' justify-content-center col'>
                <Col className='p-3' md='4' style={{ fontSize: '24px', backgroundColor: '#D6DBBB', color: '#595F3C', textAlign: 'center' }}>訂單記錄</Col>
            </Row>
            <Table hover size="sm" style={{ textAlign: 'center' }}>
                <thead>
                    <tr>
                        <th>訂單時間</th>
                        <th>店名</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>111/12/29 17:31</td>
                        <td>全好吃麵店</td>
                        <td>進行中</td>
                        <td>
                            <button className='btn' style={{ background: '#E0E0DF' }} onClick={e => navigate('/order')}>查看細項</button>
                        </td>
                    </tr>
                </tbody>
            </Table>
        </>
    )
}
export default OrderRecord;