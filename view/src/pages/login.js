// // import logo from "./logo.svg";
import { useNavigate } from "react-router-dom";
import "./style.css";
function App() {
  const navigate = useNavigate();
  return (
    <>
      <div className="col" style={{ justifyContent: 'center' }}>
        <section className="loginBox">
          <div className="row">
            <div className="top col-6">
              <h2 style={{ textAlign: 'center' }}>會員登入</h2>
            </div>
            <div className="top col-6">
              <h2 style={{ backgroundColor: "#f3f5e3", textAlign: 'center' }}>訪客登入</h2>
            </div>
          </div>
          <section className="accountBox">
            <p>帳號</p>
            <input type="text" />
          </section>
          <section className="passwordBox">
            <p>密碼</p>
            <input type="password" />
          </section>
          <section className="loginBtn">
            <button onClick={e => navigate('/home')}>登入</button>
          </section>
          <section className="registerBtn">
            <button onClick={e => navigate('/register')}>註冊</button>
          </section>
        </section>
      </div>
    </>
  );
}

export default App;
