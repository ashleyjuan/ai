import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Form, Button, Modal, Table } from 'react-bootstrap';
import { Navigate, Outlet, useNavigate, useLocation } from 'react-router-dom'
import styled from 'styled-components';
const Home = () => {
    const navigate = useNavigate();
    return (
        <>
            <Row className='justify-content-center align-items-center p-5' >
                <Col md="auto">
                    <button className='btn p-4' onClick={e => navigate('/out')} style={{ background: '#EFF1E5', color: 'black', fontSize: '24px' }}>一般者
                        （參加代購）</button>
                </Col>
                <Col md="auto">
                    <button className='btn p-4' onClick={e => navigate('/takeaway')} style={{ background: '#EFF1E5', color: 'black', fontSize: '24px' }}>外帶者
                        （發起代購）</button>
                </Col>
            </Row>
        </>
    )
}
export default Home;