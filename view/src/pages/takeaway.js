import "./takeaway.css";
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Form, Modal, Table } from 'react-bootstrap';
import { Navigate, Outlet, useNavigate, useLocation } from 'react-router-dom'
import styled from 'styled-components';
import { Spin, Upload, Button, DatePicker } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
const props = {
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  listType: 'picture',
  beforeUpload(file) {
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const img = document.createElement('img');
        img.src = reader.result;
        img.onload = () => {
          const canvas = document.createElement('canvas');
          canvas.width = img.naturalWidth;
          canvas.height = img.naturalHeight;
          const ctx = canvas.getContext('2d');
          ctx.drawImage(img, 0, 0);
          ctx.fillStyle = 'red';
          ctx.textBaseline = 'middle';
          ctx.font = '33px Arial';
          ctx.fillText('Ant Design', 20, 20);
          canvas.toBlob((result) => resolve(result));
        };
      };
    });
  },
};
const Takeaway = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [show, setShow] = useState(false);
  const navigate = useNavigate();
  const handleOnShow = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setShow(true);
    }, [10000])
  }
  useEffect(() => {
    axios.get(`/order.json`, {})
      .then(response => {
        setData(response.data);
      })
      .catch((error) => { console.error(error) })
      .finally(() => {
      })
  }, [])
  const onChange = (date, dateString) => {
    console.log(dateString);
    localStorage.setItem('date', dateString);
  };
  return (
    <>
      <Col md='auto'>
        <img onClick={e => navigate(-1)} type='button' width='auto' src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAAtCAYAAAD/aHgLAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMKSURBVHgB3ZpBTttAFIbfjE1ZlXIEHyHcgFwAYoUidYUilVRdNTlB4QSFFSpBSrpCagmTcAF6A3KD+ghhWdWe6TwnQXXwTJy4Is/+Nons8eLpef557/0G+A9ci67XH1wKIAyHnHRFd3uTR/eMsVp/2PkMRMkd6JbzpwugPPzPAE5uh1dHQBAGORA6g0oHN3d5HEpVPfSbI1gDP8TFbtr1lQPti68txvmX9Lsq+C2j6jv/YwAviBAXnuLur7R7K726KD46SMt+ZN4r4DUgxNKBYpAoPvrvtmmNBHVe9z+cASFcWJJNHuIx4plXqOBgv9kCYiyVUX184J6smFeogOm9CQTJHCgqrFYua6aYBN9/YQHKSqZAv4vLSsoxkkAxaPtrOlKysDBQFB+Xc2t5FwGc1veOSYnPPNZAZ+XdrPIxMHq7f3wCxLGq7mseYpY884pYfHwoAMZAp+WdrW4do8JSFZ95Ul/dG3FVWyg+UrWLEiTyLFAUH85V1/ZQLD5+swcFIhFolvKOMTUogvjMk9ijmyzETHqW9eMoUt9MrdC6keB4pnbs6bqhtywNcaD23rIcMCzvXM4eoOTwDYeVOpMzOESqrX/HUHI4dhy68ziFkvOkunpMqeta9QlKSuLYEXede6Vg1/qAZLqIV4V71ZNFfeT6wKMHW1smHXlU32sWomP5l0QJ6PsN3ZE4OPMxZowp2taDiWdFvQ42kJI1bA9Rth5MpLZpB/77gS4HFyixOsNiAwqC1ZK4GXZ63Np8q+BRbuw09CsPxLHOjBzp4njTMtlj3hsnJO2LzrAGOhEnFxXWnDF9HE0H26RZOO5EccLBtG0NDraxAwLCZLYN+3edFlNgzZz2RXcOiQ6xM1sSkwE1O7etcTmIa+1RAkGWNoJvhx3sXW3HyuhRulVqSry0PzoRJxZYllS2eEiuclrJ2hdoOnFmnRbquW+D0kh0JWs/7mFl3LAbUIHu6H8CIVb+/AazZSgTSVoVuT6/QeZ7WPRJKVqIuT+oinvYqTgVwSfNhdBWhj52ekCYvzQBKODfKil1AAAAAElFTkSuQmCC" />
      </Col>
      <section class="takeaway">
        <br />
        <h2 class="blue">發起代購</h2>
        <br />
        <button class="btn lightblue" onClick={e => navigate('/purchasing')}>查看已發起的代購</button>
        <br />
      </section>
      <section class="takeaway">
        <h2 class="red">新增代購</h2>
        <br />
      </section>
      <section class="takeaway">
        <p>上傳菜單</p>
        {/* <button class="btn lightred">選擇檔案</button> */}
        <Upload {...props}>
          <Button icon={<UploadOutlined />} onClick={handleOnShow}>選擇檔案</Button>
        </Upload>
      </section>
      {show ?
        <section class="takeaway">
          <p>預覽</p>
          {loading ?
            <div style={{
              margin: '20px 0',
              marginBottom: '20px',
              padding: '30px 50px',
              textAlign: ' center',
              background: 'rgba(0, 0, 0, 0.05)',
              borderRadius: '4px'
            }}>
              <Spin></Spin>
            </div>
            :
            <>
              <table>
                <thead>
                  <tr>
                    <th>品名</th>
                    <th>單價</th>
                    <th>數量上限</th>
                    {/* <th></th> */}
                  </tr>
                </thead>
                <tbody>
                  {data.map((tmp, index) => {
                    return (
                      <tr>
                        <td>{tmp['name']}</td>
                        <td>{tmp['price']}</td>
                        <td>
                          <select defaultValue={''}>
                            <option disabled value={''}>請選擇</option>
                            <option value={1}>1</option>
                            <option value={2}>2</option>
                            <option value={3}>3</option>
                            <option value={4}>4</option>
                            <option vlaue={5}>5</option>
                          </select>
                        </td>
                        {/* <td>
                          <button class="btnmini grey">修改</button>
                          {/* <button class="btnmini pink">刪除</button> */}
                        {/* </td> */}
                      </tr>
                    )
                  })}
                </tbody>
              </table>
            </>}

        </section>
        : null}
      <section class="takeaway">
        <p>訂單截止時間</p>
        <DatePicker onChange={onChange} />
        <button class="btn lightred" onClick={e => navigate('/takeaway2')}>確認</button>
      </section>
    </>
  );
};
export default Takeaway;
