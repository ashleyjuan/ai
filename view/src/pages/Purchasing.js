import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Form, Button, Modal, Table } from 'react-bootstrap';
import { Navigate, Outlet, useNavigate, useLocation } from 'react-router-dom'
import styled from 'styled-components';
import "./purchasing.css";

const Purchasing = () => {
  const navigate = useNavigate();
  const [data, setData] = useState([{
    key: 1,
    time: '111/10/29 18:30',
    name: '銷魂麵舖',
    qrcode: 'FJ3918JKSF'
  }, {
    key: 2,
    time: '111/10/20 17:30',
    name: '上海生煎湯包',
    qrcode: 'J91SKJFG93'
  }, {
    key: 3,
    time: '111/10/11 12:00',
    name: '鶴茶樓',
    qrcode: 'JPQ29KFC62'
  }]);
  const remove = (key) => {
    const newList = data.filter((item) => item.key !== key)
    setData(newList);
  }
  return (
    <>
      <Col md='auto'>
        <img onClick={e => navigate(-1)} type='button' width='auto' src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAAtCAYAAAD/aHgLAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMKSURBVHgB3ZpBTttAFIbfjE1ZlXIEHyHcgFwAYoUidYUilVRdNTlB4QSFFSpBSrpCagmTcAF6A3KD+ghhWdWe6TwnQXXwTJy4Is/+Nons8eLpef557/0G+A9ci67XH1wKIAyHnHRFd3uTR/eMsVp/2PkMRMkd6JbzpwugPPzPAE5uh1dHQBAGORA6g0oHN3d5HEpVPfSbI1gDP8TFbtr1lQPti68txvmX9Lsq+C2j6jv/YwAviBAXnuLur7R7K726KD46SMt+ZN4r4DUgxNKBYpAoPvrvtmmNBHVe9z+cASFcWJJNHuIx4plXqOBgv9kCYiyVUX184J6smFeogOm9CQTJHCgqrFYua6aYBN9/YQHKSqZAv4vLSsoxkkAxaPtrOlKysDBQFB+Xc2t5FwGc1veOSYnPPNZAZ+XdrPIxMHq7f3wCxLGq7mseYpY884pYfHwoAMZAp+WdrW4do8JSFZ95Ul/dG3FVWyg+UrWLEiTyLFAUH85V1/ZQLD5+swcFIhFolvKOMTUogvjMk9ijmyzETHqW9eMoUt9MrdC6keB4pnbs6bqhtywNcaD23rIcMCzvXM4eoOTwDYeVOpMzOESqrX/HUHI4dhy68ziFkvOkunpMqeta9QlKSuLYEXede6Vg1/qAZLqIV4V71ZNFfeT6wKMHW1smHXlU32sWomP5l0QJ6PsN3ZE4OPMxZowp2taDiWdFvQ42kJI1bA9Rth5MpLZpB/77gS4HFyixOsNiAwqC1ZK4GXZ63Np8q+BRbuw09CsPxLHOjBzp4njTMtlj3hsnJO2LzrAGOhEnFxXWnDF9HE0H26RZOO5EccLBtG0NDraxAwLCZLYN+3edFlNgzZz2RXcOiQ6xM1sSkwE1O7etcTmIa+1RAkGWNoJvhx3sXW3HyuhRulVqSry0PzoRJxZYllS2eEiuclrJ2hdoOnFmnRbquW+D0kh0JWs/7mFl3LAbUIHu6H8CIVb+/AazZSgTSVoVuT6/QeZ7WPRJKVqIuT+oinvYqTgVwSfNhdBWhj52ekCYvzQBKODfKil1AAAAAElFTkSuQmCC" />
      </Col>
      <section class="title">
        <h2>查看已發起代購</h2>
      </section>
      <section class="Table">
        <table>
          <colgroup span="4"></colgroup>
          <tr>
            <th>訂單時間</th>
            <th>店名</th>
            <th>代碼</th>
            <th></th>
          </tr>
          {data.map((data, item) => {
            return (
              <tr>
                <td>{data['time']}</td>
                <td>{data['name']}</td>
                <td>{data['qrcode']}</td>
                <td>
                  <a onClick={e => navigate("/purchasing1")} class="detail">
                    查看細項
                  </a>
                  <a class="delete" onClick={() => remove(data['key'])}>
                    刪除
                  </a>
                </td>
              </tr>
            )
          })

          }
        </table>
      </section>
    </>
  );
};
export default Purchasing;
