// import "./takeaway2.css";
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Form, Button, Modal, Table } from 'react-bootstrap';
import { Navigate, Outlet, useNavigate, useLocation } from 'react-router-dom'
import styled from 'styled-components';
const Takeaway2 = () => {
  const [show, setShow] = useState(false);
  const navigate = useNavigate();
  const [date, setDate] = useState('');
  const [data, setData] = useState([]);
  useEffect(() => {
    const date = localStorage.getItem('date');
    setDate(date);
  }, [])
  useEffect(() => {
    axios.get(`/order.json`, {})
      .then(response => {
        // response.data.map((tmp, index) => {
        //   if ()
        // })
        setData(response.data);
      })
      .catch((error) => { console.error(error) })
      .finally(() => {
      })
  }, [])
  return (
    <>
      <Col md='auto'>
        <img onClick={e => navigate(-1)} type='button' width='auto' src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAAtCAYAAAD/aHgLAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMKSURBVHgB3ZpBTttAFIbfjE1ZlXIEHyHcgFwAYoUidYUilVRdNTlB4QSFFSpBSrpCagmTcAF6A3KD+ghhWdWe6TwnQXXwTJy4Is/+Nons8eLpef557/0G+A9ci67XH1wKIAyHnHRFd3uTR/eMsVp/2PkMRMkd6JbzpwugPPzPAE5uh1dHQBAGORA6g0oHN3d5HEpVPfSbI1gDP8TFbtr1lQPti68txvmX9Lsq+C2j6jv/YwAviBAXnuLur7R7K726KD46SMt+ZN4r4DUgxNKBYpAoPvrvtmmNBHVe9z+cASFcWJJNHuIx4plXqOBgv9kCYiyVUX184J6smFeogOm9CQTJHCgqrFYua6aYBN9/YQHKSqZAv4vLSsoxkkAxaPtrOlKysDBQFB+Xc2t5FwGc1veOSYnPPNZAZ+XdrPIxMHq7f3wCxLGq7mseYpY884pYfHwoAMZAp+WdrW4do8JSFZ95Ul/dG3FVWyg+UrWLEiTyLFAUH85V1/ZQLD5+swcFIhFolvKOMTUogvjMk9ijmyzETHqW9eMoUt9MrdC6keB4pnbs6bqhtywNcaD23rIcMCzvXM4eoOTwDYeVOpMzOESqrX/HUHI4dhy68ziFkvOkunpMqeta9QlKSuLYEXede6Vg1/qAZLqIV4V71ZNFfeT6wKMHW1smHXlU32sWomP5l0QJ6PsN3ZE4OPMxZowp2taDiWdFvQ42kJI1bA9Rth5MpLZpB/77gS4HFyixOsNiAwqC1ZK4GXZ63Np8q+BRbuw09CsPxLHOjBzp4njTMtlj3hsnJO2LzrAGOhEnFxXWnDF9HE0H26RZOO5EccLBtG0NDraxAwLCZLYN+3edFlNgzZz2RXcOiQ6xM1sSkwE1O7etcTmIa+1RAkGWNoJvhx3sXW3HyuhRulVqSry0PzoRJxZYllS2eEiuclrJ2hdoOnFmnRbquW+D0kh0JWs/7mFl3LAbUIHu6H8CIVb+/AazZSgTSVoVuT6/QeZ7WPRJKVqIuT+oinvYqTgVwSfNhdBWhj52ekCYvzQBKODfKil1AAAAAElFTkSuQmCC" />
      </Col>
      <section class="takeaway">
        <br />
        <h2 class="red">代購資料確認</h2>
        <br />
      </section>
      <section class="takeaway">
        <table>
          <thead>
            <tr>
              <th>品名</th>
              <th>單價</th>
              <th>數量上限</th>
              {/* <th>備註</th> */}
            </tr>
          </thead>
          <tbody>
            {data.map((tmp, index) => {
              if (tmp['num'] !== 0) {
                return (
                  <tr>
                    <td>{tmp['name']}</td>
                    <td>{tmp['price']}</td>
                    <td>{tmp['num']}</td>
                    {/* <td>{tmp['note']}</td> */}
                  </tr>
                )
              }
            })}
          </tbody>
          <tfoot></tfoot>
        </table>
      </section>
      <section class="takeaway">
        <p>訂單截止時間：{date} 18:30</p>
      </section>
      <section class="takeaway">
        <button class="btn lightred" onClick={e => navigate(-1)}>返回</button>
        <button class="btn lightred" onClick={e => setShow(true)}>確認</button>
      </section>
      <Modal show={show} onHide={e => setShow(false)}>
        <Modal.Header className='justify-content-center' style={{ background: '#FAEFE4' }}>
          <Modal.Title style={{ fontSize: '24px' }}>發起代購成功</Modal.Title>
        </Modal.Header>
        <Modal.Body >
          <Col style={{ textAlign: 'center', padding: '30px', fontSize: '24px' }}>訂單代碼：VD38JN1KC</Col>
          {/* <Modal.Footer className='justify-content-center'> */}
          <Row className='justify-content-center'>
            <Col md="auto">
              <button className='btn' onClick={e => setShow(false)} style={{ background: '#FAEFE4', color: 'black' }}>確認</button>
            </Col>
          </Row>
        </Modal.Body>
        {/* </Modal.Footer> */}
      </Modal>
    </>
  );
};
export default Takeaway2;
