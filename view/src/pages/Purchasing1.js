import "./purchasing1.css";
import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Form, Button, Modal, Table } from 'react-bootstrap';
import { Navigate, Outlet, useNavigate, useLocation } from 'react-router-dom'
import { Input } from "antd";
const Purchasing1 = () => {
  const navigate = useNavigate();
  const [sum, setSum] = useState([160, 200, 180]);
  const [drink1, setDrink1] = useState(false);
  const [drink2, setDrink2] = useState(false);
  const [drink3, setDrink3] = useState(false);
  const [drink1sum, setDrink1sum] = useState(3);
  const [drink2sum, setDrink2sum] = useState(2);
  const [drink3sum, setDrink3sum] = useState(10);
  const [i, setI] = useState(0);
  const getSum = () => {
    setI(i + 1);
  }
  return (
    <>
      <Col md='auto'>
        <img onClick={e => navigate(-1)} type='button' width='auto' src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAAtCAYAAAD/aHgLAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMKSURBVHgB3ZpBTttAFIbfjE1ZlXIEHyHcgFwAYoUidYUilVRdNTlB4QSFFSpBSrpCagmTcAF6A3KD+ghhWdWe6TwnQXXwTJy4Is/+Nons8eLpef557/0G+A9ci67XH1wKIAyHnHRFd3uTR/eMsVp/2PkMRMkd6JbzpwugPPzPAE5uh1dHQBAGORA6g0oHN3d5HEpVPfSbI1gDP8TFbtr1lQPti68txvmX9Lsq+C2j6jv/YwAviBAXnuLur7R7K726KD46SMt+ZN4r4DUgxNKBYpAoPvrvtmmNBHVe9z+cASFcWJJNHuIx4plXqOBgv9kCYiyVUX184J6smFeogOm9CQTJHCgqrFYua6aYBN9/YQHKSqZAv4vLSsoxkkAxaPtrOlKysDBQFB+Xc2t5FwGc1veOSYnPPNZAZ+XdrPIxMHq7f3wCxLGq7mseYpY884pYfHwoAMZAp+WdrW4do8JSFZ95Ul/dG3FVWyg+UrWLEiTyLFAUH85V1/ZQLD5+swcFIhFolvKOMTUogvjMk9ijmyzETHqW9eMoUt9MrdC6keB4pnbs6bqhtywNcaD23rIcMCzvXM4eoOTwDYeVOpMzOESqrX/HUHI4dhy68ziFkvOkunpMqeta9QlKSuLYEXede6Vg1/qAZLqIV4V71ZNFfeT6wKMHW1smHXlU32sWomP5l0QJ6PsN3ZE4OPMxZowp2taDiWdFvQ42kJI1bA9Rth5MpLZpB/77gS4HFyixOsNiAwqC1ZK4GXZ63Np8q+BRbuw09CsPxLHOjBzp4njTMtlj3hsnJO2LzrAGOhEnFxXWnDF9HE0H26RZOO5EccLBtG0NDraxAwLCZLYN+3edFlNgzZz2RXcOiQ6xM1sSkwE1O7etcTmIa+1RAkGWNoJvhx3sXW3HyuhRulVqSry0PzoRJxZYllS2eEiuclrJ2hdoOnFmnRbquW+D0kh0JWs/7mFl3LAbUIHu6H8CIVb+/AazZSgTSVoVuT6/QeZ7WPRJKVqIuT+oinvYqTgVwSfNhdBWhj52ekCYvzQBKODfKil1AAAAAElFTkSuQmCC" />
      </Col>
      {/* <section class="title"> */}
      <div className="justify-content-center p-2">
        <Col md="3" style={{ background: '#CADCEA' }}>
          <h2 >查看細項</h2>
        </Col>
      </div>
      <h3>店家名稱：鶴茶樓</h3>
      <h3>截止時間：111 / 10 / 11 12:00</h3>
      <h3>總金額：{sum[i]}元</h3>
      {/* </section> */}
      <section class="Table">
        <table>
          <colgroup span="4"></colgroup>
          <tr>
            <th>品名</th>
            <th>總數</th>
            <th></th>
            <th>訂購人姓名</th>
          </tr>
          <tr>
            <td>綺夢紅茶</td>
            <td>{drink1 ?
              <>
                <Form.Select value={drink1sum} onChange={e => setDrink1sum(e.target.value)}>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                </Form.Select>
              </> :
              drink1sum
            }</td>
            <td>
              {drink1 ?
                <button className='btn' style={{ background: '#ECCFCF' }} onClick={e => { setDrink1(false); getSum() }}>
                  完成
                </button> :
                <button className='btn' style={{ background: '#ECCFCF' }} onClick={e => setDrink1(true)}>
                  修改
                </button>
              }
            </td>
            <td>花花、泡泡</td>
          </tr>
          <tr>
            <td>桂香烏龍茶</td>
            <td>
              {drink2 ?
                <>
                  <Form.Select value={drink2sum} onChange={e => setDrink2sum(e.target.value)}>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </Form.Select>
                </> :
                drink2sum
              }
            </td>
            <td>
              {drink2 ?
                <button className='btn' style={{ background: '#FAEFE4' }} onClick={e => { setDrink2(false); getSum() }}>
                  完成
                </button> :
                <button className='btn' style={{ background: '#ECCFCF' }} onClick={e => setDrink2(true)}>
                  修改
                </button>
              } </td>
            <td>花花</td>
          </tr>
          <tr>
            <td>鶴頂那堤</td>
            <td>{drink3 ?
              <>
                <Form.Select value={drink3sum} onChange={e => setDrink3sum(e.target.value)}>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                  <option>7</option>
                  <option>8</option>
                  <option>9</option>
                  <option>10</option>
                </Form.Select>
              </> :
              drink3sum
            }</td>
            <td>{drink3 ?
              <button className='btn' style={{ background: '#FAEFE4' }} onClick={e => { setDrink3(false); getSum() }}>
                完成
              </button> :
              <button className='btn' style={{ background: '#ECCFCF' }} onClick={e => setDrink3(true)}>
                修改
              </button>}
            </td>
            <td>Jenny、毛毛、泡泡</td>
          </tr>
        </table>
      </section>
    </>
  );
};
export default Purchasing1;
