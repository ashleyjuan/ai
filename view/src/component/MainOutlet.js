import { Breadcrumb, Layout, Menu, Image, Row, Col } from 'antd';
import React, { useEffect, useState } from 'react';
import { Navigate, Outlet, useNavigate, useLocation } from 'react-router-dom'
import "antd/dist/antd.css";
import styled from 'styled-components';
const { Header, Footer, Content } = Layout;

const MainOutlet = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const [menu_sider, setMenuSider] = useState([]);
    const [selecte_keys, setSelectedKeys] = useState(window.location.pathname);
    const titles = {
        '/home': '首頁',
        '/out': '一般者',
        '/takeaway': '外帶者',
        '/': '登出',
    }
    const onSelect = (e) => {
        navigate(e.key)
        document.title = titles[e.key]
        setSelectedKeys([e.key])
    }
    return (
        // <Layout className="layout" style={{ background: '#D6DBBB' }} >

        //     <Header >
        //         <Menu
        //             theme="dark"
        //             mode="horizontal"
        //             defaultSelectedKeys={['/']}
        //             selectedKeys={selecte_keys}
        //             onSelect={onSelect}
        //             items={[
        //                 {
        //                     key: "/",
        //                     label: `首頁`,
        //                 },
        //                 {
        //                     key: "/person",
        //                     label: `一般者`,
        //                 },
        //                 {
        //                     key: "/takeperson",
        //                     label: `外帶者`,
        //                 }
        //             ]}
        //         />
        //     </Header>
        //     <Content
        //         style={{
        //             padding: '30px 50px',
        //             fontSize: "18px"
        //         }}
        //     >
        //         <Row>
        //             <Col span={24}>
        //                 <Outlet />
        //             </Col>
        //         </Row>
        //     </Content>
        //     <Footer
        //         style={{
        //             textAlign: 'center',
        //         }}
        //     >
        //         Copyright ©2022 高師軟體侯團隊
        //     </Footer>
        // </Layout>
        <Layout className="layout">
            <Header>
                <Menu
                    theme="dark"
                    style={{ background: '#D6DBBB' }}
                    mode="horizontal"
                    defaultSelectedKeys={['/']}
                    selectedKeys={selecte_keys}
                    onSelect={onSelect}
                    items={[
                        {
                            key: "/home",
                            label: `首頁`,
                        },
                        {
                            key: "/out",
                            label: `一般者`,
                        },
                        {
                            key: "/takeaway",
                            label: `外帶者`,
                        },
                        {
                            key: "/",
                            label: `登出`,
                        }
                    ]}
                />
            </Header>
            <Content
                style={{
                    padding: '30px 50px',
                    fontSize: "18px",
                    background: '#F5F5F5'
                }}
            >
                <Row>
                    <Col span={24}>
                        <Outlet />
                    </Col>
                </Row>
            </Content>
            <Footer
                style={{
                    textAlign: 'center',
                    background: '#FFFFFF'
                }}
            >
                Copyright ©2022 高師軟體猴團隊
            </Footer>
        </Layout >
    );
};
export default MainOutlet;
